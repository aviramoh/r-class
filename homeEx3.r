students <- read.csv('student-mat.csv', sep = ";")

str(students)

#check if there are any na's
any(is.na(students))


#Taking numeric columns 
cols.numeric <- sapply(students,is.numeric)

#students with only numeric data 
students.numeric <- students[,cols.numeric]

#compute correlations 
cor.data <- cor(students.numeric)

#Show correlations 
install.packages("corrplot")
library(corrplot)

print(corrplot(cor.data, method = 'color'))

plot(students$G1,students$G3)
plot(students$activities,students$G3 )


hist(students[,'G3'], col = 'blue')

#split into trainig set and test set 
install.packages("caTools")
library(caTools)

set.seed(101)
sample <- sample.split(students$G3, SplitRatio = 0.7) #can use any column

train <- subset(students, sample)
test <- subset(students, !sample)

nrow(students)
nrow(train)
nrow(test)
nrow(test) + nrow(train)

#Compute linear model data
# model<-lm(y ~ x1+x2..) x1 and x2
# model lm(y ~ .)

model <- lm(G3 ~ ., data = train)

print(summary(model1))


#data is not mormalized
res <- residuals(model1) #����� ��� ������ ������ ������� ����
hist(res, col = 'blue')

#Making predictions on the test set 
G3.predicted <- predict(model,test)


#computing error on the test set
results <- cbind(G3.predicted, test$G3)

colnames(results) <- c('Predicted', 'Actual')

results <- as.data.frame(results)

#Cutting negative values 
to_zero <- function(x){
  if(x<0){
    return (0)
  } else {
    return (x)
  }
}

results$Predicted <- sapply(results$Predicted,to_zero)

min(results$Predicted)


#Mean square error
MSE <- mean((results$Actual-results$Predicted)^2)
RMSE <- MSE^0.5

#Computing Overfitting effect 
G3.predicted.train <- predict(model,train)

results.train <- cbind(G3.predicted.train, train$G3)

colnames(results.train) <- c('Predicted', 'Actual')

results.train <- as.data.frame(results.train)

results.train$Predicted <- sapply(results.train$Predicted,to_zero)

MSE.train <- mean((results.train$Actual-results.train$Predicted)^2)
RMSE.train <- MSE.train^0.5

overfitting <- ((RMSE - RMSE.train)/RMSE)*100

str(students)


