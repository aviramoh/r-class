7
x <- 5
x[1]
x <- c(1,3,5,6,7,8)
class(x)
y <- as.integer(x)
z <- as.character(x)

#subsetting and sequence

a <- 1:3 ##always will be integer

remove(x)

?remove

b <- c(17,22,45,78)

b[3]

b[3:4]

b[c(3:4,3:4)]

b[c(1,4)]

#names for items in a vector

salary <- c(800,6000,4000,4000)

names(salary) <- c('Alice', 'Jane', 'Jerry', 'Jack')

salary['Alice']

names(salary)
salary[1:3]

#logical operators

x <- c(4,6,100,600)

z <- x > 7 #check for each array value if it greater than 7

x[z]

x[x>7]

x[!z]

x[-2]

x[-3]

x[c(-2,-3)]

rev(x)

z <- c(8,9,4,5)

sort(z)

sort(z, decreasing = T)

order(z)

order(-z)

z[order(z)]

z[z]

seq(1,4,length = 7)

seq(1,4,length = 18)

x <- c(1,19,23)

sample(x,10, replace=T)

sample(x,10)

x <- c(x,NA)

?mean

example(mean)

mean(x)

mean(x,na.rm = T)

sum(x,na.rm = T)

notNA <- !is.na(x)

x[notNA]

#metrices

?matrix

y <- matrix(c(4,6,7,9,3,9),nrow = 2, ncol = 3)

y <- matrix(c(4,6,7,9,3,9),nrow = 2, ncol = 3, byrow = T)

y[1,2]

y[2,]

y[,3]

y[,-2] #all columns besides 2nd column

v1 <- c(1,2,3,4,5)
v2 <- c(5,6,7,8,9)

mat1 <- rbind(v1,v2)
mat2 <- cbind(v1,v2)

mat1[6] #by column
mat2[6] #always by columns
